//https://stackoverflow.com/questions/1102891/how-to-check-if-a-string-is-numeric-in-java/29331473
//https://gist.github.com/Gaurav-Jayswal/9228965
//https://www.101computing.net/reverse-polish-notation/
//https://www.tabnine.com/code/java/methods/net.sf.saxon.dom.DOMNodeList/%3Cinit%3E

import java.util.*;

public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   private static boolean isDigit(Tnode node) { //https://stackoverflow.com/questions/1102891/how-to-check-if-a-string-is-numeric-in-java/29331473
      try {
         Integer.parseInt(node.name); //Возвращает true, если все символы в строке являются цифрами и есть хотя бы один символ, иначе false.
         return true;
      } catch (RuntimeException e) {
         return false;
      }
   }

   private static List<Tnode> newList(List<String> stringList) { //https://www.tabnine.com/code/java/methods/net.sf.saxon.dom.DOMNodeList/%3Cinit%3E
      List<Tnode> tList = new ArrayList<>(); //Создает пустой список для узлов дерева.
      for (String str : stringList) { //Для каждой строки из параметра stringList создает узел дерева с текстом из этой строки и помещает созданный узел в список узлов.
         tList.add(new Tnode(str, null, null));
      }
      return tList; //Возвращает полученный список узлов в качестве результата туда, где метод был вызван..
   }


   @Override
   public String toString() {
      StringBuffer b = new StringBuffer();
      List<Tnode> usedList = new ArrayList<>();
      Tnode current = this;

      do {
         if (!usedList.contains(current)) {
            b.append(current.getName());
            usedList.add(current);
         }

         Tnode child = current.getFirstChild();
         Tnode sibling = current.getNextSibling();

         if (child != null) {
            if (child.getNextSibling() != null || child.getFirstChild() != null) {
               current = current.getFirstChild();
            } else {
               current.setFirstChild(null);
               current = this;
            }
            if (!usedList.contains(current)) {
               b.append("(");
            }
         } else if (null != sibling) {
            if (sibling.getFirstChild() != null) {
               current = current.getNextSibling();
            } else {
               current.setNextSibling(null);
               if (!usedList.contains(sibling)) b.append(",").append(sibling.getName());
               current = this;
               b.append(")");
            }
            if (!usedList.contains(current)) {
               b.append(",");
            }
         }
      } while (this.getFirstChild() != null);
      return b.toString();
   }

    private Tnode(String n, Tnode down, Tnode right) {
       name = n;
       firstChild = down;
       nextSibling = right;
    }

   private String getName() {
      return name;
   }

   private Tnode getFirstChild() {
      return firstChild;
   }

   private void setFirstChild(Tnode down) {
      firstChild = down;
   }

   private Tnode getNextSibling() {
      return nextSibling;
   }

   private void setNextSibling(Tnode right) {
      nextSibling = right;
   }

   private static boolean isSymbol(Tnode node) {
      return (Arrays.asList("*", "/", "+", "-","DUP","SWAP","ROT").contains(node.name));
   }


   public static Tnode buildFromRPN (String pol) {
      if (pol.equals("")) {
         throw new RuntimeException(String.format("empty subtree %s",
                 pol));
      }
      List<String> symbolList = Arrays.asList(pol.split(" "));
      List<Tnode> tList = newList(symbolList);
      int counter = 0;
      if (tList.size() == 1 && !isDigit(tList.get(0))) {
         throw new RuntimeException(String.format("Invalid %s symbol ",
                 pol));
      }

      for (Tnode tnode : tList) {
         if (!isDigit(tnode))
            if (!isSymbol(tnode)) {
               throw new RuntimeException(String.format("Statement %s has invalid symbol ",
                       pol));
            }
      }

      while (tList.size() > 1) { //Here started Swap/Rot/Dup tasks
         if (tList.size() < 3) {
            throw new RuntimeException(String.format("There are not enough elements in the stack to complete %s",
                    pol));
         }

            if (tList.size() > counter){
            if (Objects.equals(tList.get(counter).name, "DUP")) {
               if(tList.size() < 1) {
                  throw new RuntimeException(String.format("There are not enough elements in the stack to complete %s",
                          pol));
               }
               Tnode fC = tList.get(counter - 1);
               Tnode nS = clone(fC);
               fC.setNextSibling(nS);
               tList.set(counter,nS);
               counter = 0;
               continue;
            }
            else if (Objects.equals(tList.get(counter + 2).name, "DUP")) {
               Tnode fC = tList.get(counter + 1);
               Tnode nS = clone(fC);
               fC.setNextSibling(nS);
               tList.set(counter + 2,nS);
               counter = 0;
               continue;
            }
         }

            if (tList.size() > counter + 2){
            if (Objects.equals(tList.get(counter + 2).name, "SWAP")){
               if (tList.size() < 2){
                  throw new RuntimeException(String.format("There are not enough elements in the stack to complete %s",
                          pol));
               }
               Tnode fC = tList.get(counter + 1);
               Tnode pL = tList.get(counter);
               tList.set(counter + 1,pL);
               tList.set(counter,fC);
               tList.remove(counter + 2);
               counter = 0;
               continue;
            }
         }
            if (tList.size() > counter + 3){
            if (Objects.equals(tList.get(counter + 3).name, "ROT")){
               if (tList.size() < 3){
                  throw new RuntimeException(String.format("There are not enough elements in the stack to complete %s",
                          pol));
               }
               Tnode fC = tList.get(counter + 2);
               Tnode pL = tList.get(counter + 1);
               Tnode lL = tList.get(counter);
               tList.set(counter,pL);
               tList.set(counter + 1,fC);
               tList.set(counter + 2,lL);
               tList.remove(counter + 3);
               counter = 0;
               continue;
            }
         }

         if (tList.size() > 9) {
            throw new RuntimeException(String.format("There are too much elements in the stack to complete %s",
                    pol));
         }
         if (pol.contains("[!@#$%^&?,.]+") || (pol.contains("[A-Za-z]+"))) {
            throw new RuntimeException(String.format("%s contains illegal arguments",
                    pol));
         }

         if (isDigit(tList.get(counter))) {
            if (isDigit(tList.get(counter + 1))) {
               if (!isDigit(tList.get(counter + 2)))
                  if (tList.get(counter + 2).getFirstChild() == null) {
                     Tnode top = tList.get(counter + 2);
                     Tnode left = tList.get(counter);
                     Tnode right = tList.get(counter + 1);
                     left.setNextSibling(right);
                     top.setFirstChild(left);
                     tList.remove(counter + 1);
                     tList.remove(counter);
                     counter = 0;
                     continue;
                  }
            } else if (tList.get(counter + 1).getFirstChild() != null) {
               if (!isDigit(tList.get(counter + 2)))
                  if (tList.get(counter + 2).getFirstChild() == null) {
                     Tnode top = tList.get(counter + 2);
                     Tnode left = tList.get(counter);
                     Tnode right = tList.get(counter + 1);
                     left.setNextSibling(right);
                     top.setFirstChild(left);
                     tList.remove(counter + 1);
                     tList.remove(counter);
                     counter = 0;
                     continue;
                  }
            }
         } else if (tList.get(counter).getFirstChild() != null) {
            if (isDigit(tList.get(counter + 1)) || tList.get(counter + 1).getFirstChild() != null) {
               if (!isDigit(tList.get(counter + 2)) && tList.get(counter + 2).getFirstChild() == null) {
                  Tnode top = tList.get(counter + 2);
                  Tnode left = tList.get(counter);
                  Tnode right = tList.get(counter + 1);
                  left.setNextSibling(right);
                  top.setFirstChild(left);
                  tList.remove(counter + 1);
                  tList.remove(counter);
                  counter = 0;
                  continue;
               }
            }
         }
         counter = counter + 1;
      }
      return tList.get(0);
   }

   public static Tnode clone(Tnode node) {
      Tnode nodeNew = null;
      if (node.nextSibling != null)
         if (node.firstChild != null) {
            nodeNew = new Tnode(node.name,
                    clone(node.firstChild),
                    clone(node.nextSibling));
         }
      if (node.firstChild == null)
         if (node.nextSibling != null) {
            nodeNew = new Tnode(node.name,
                    null,
                    clone(node.nextSibling));
         }
      if (node.nextSibling == null)
         if (node.firstChild != null) {
            nodeNew = new Tnode(node.name,
                    clone(node.firstChild),
                    null);
         }
      if (node.firstChild == null)
         if (node.nextSibling == null) {
            nodeNew = new Tnode(node.name,
                    null,
                    null);
         }
      return nodeNew;
   }

   public static void main (String[] param) {
      String rpn = "2 5 SWAP -" ;
      System.out.println ("RPN: " + rpn);
      Tnode res = buildFromRPN (rpn);
      System.out.println ("Tree: " + res);

      String rpn1 =  "3 DUP *" ;
      System.out.println ("RPN: " + rpn1);
      Tnode res1 = buildFromRPN (rpn1);
      System.out.println ("Tree: " + res1);

      String rpn2 = "2 5 9 ROT - +" ;
      System.out.println ("RPN: " + rpn2);
      Tnode res2 = buildFromRPN (rpn2);
      System.out.println ("Tree: " + res2);

      String rpn3 = "2 5 9 ROT + SWAP -" ;
      System.out.println ("RPN: " + rpn3);
      Tnode res3 = buildFromRPN (rpn3);
      System.out.println ("Tree: " + res3);

      String rpn4 = "2 5 DUP ROT - + DUP *" ;
      System.out.println ("RPN: " + rpn4);
      Tnode res4 = buildFromRPN (rpn4);
      System.out.println ("Tree: " + res4);

      String rpn5 = "-3 -5 -7 ROT - SWAP DUP * +" ;
      System.out.println ("RPN: " + rpn5);
      Tnode res5 = buildFromRPN (rpn5);
      System.out.println ("Tree: " + res5);
   }
}


